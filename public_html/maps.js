/*
 * Copyright 2014 Muhammet Hüseyin Olgun
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

var map;
var nodeCount = 0;
var markers = new Array();
var directionsService = new google.maps.DirectionsService();
var ways = [];
var speed = 10;
var maxPheremone = 12;

var lineSymbol = {
    path: google.maps.SymbolPath.CIRCLE,
    scale: 8,
    strokeColor: '#393'
};

function addAnt() {
    var ant = {
        offset: 0,
        visited: ["0"]
    };
    var way = detectNewWay(wayAlternativeTitles("0"), "0");
    way.ants.push(ant);
}

function start() {
    for (var i = 0; i < markers.length; i++) {
        addAnt();
    }
    animateCircle();
}

function initialize() {

    var elazig = new google.maps.LatLng(38.680426, 39.225054);

    var mapOptions = {
        zoom: 13,
        center: elazig
    };

    map = new google.maps.Map(document.getElementById('map-canvas'),
            mapOptions);

    addMarker(map, elazig, nodeCount.toString(), true);

    google.maps.event.addListener(map, 'click', function (event) {
        addMarker(map, event.latLng, (++nodeCount).toString(), false);
        for (var i = 0; i < markers.length - 1; i++) {
            calcAndDrawRoute(markers[nodeCount], markers[i], function (pl, totalDistance, to) {
                var from = markers[nodeCount].getTitle();
                var way = {
                    from: from,
                    to: to,
                    polyline: pl,
                    pheremone: 0,
                    distance: totalDistance,
                    ants: []
                };
                ways.push(way);
                var path = pl.getPath();
                path.getArray().reverse();
                pl.set('path', path);
                var newWay = {
                    from: to,
                    to: from,
                    polyline: pl,
                    pheremone: 0,
                    distance: totalDistance,
                    ants: []
                };
                ways.push(newWay);
            });
        }
    });
}


function calcAndDrawRoute(start, end, fn) {
    var request = {
        origin: start.getPosition(),
        destination: end.getPosition(),
        travelMode: google.maps.TravelMode.DRIVING
    };
    directionsService.route(request, function (response, status) {
        if (status === google.maps.DirectionsStatus.OK) {
            addPathToPolyLine(response, function (polyline, totalDistance) {
                fn(polyline, totalDistance, end.getTitle());
            });
            //renderDirection(response);
        }
    });
}

//Adds a marker to map
function addMarker(map, pos, title, startPoint) {
    var marker = new google.maps.Marker({
        position: pos,
        map: map,
        title: title,
        draggable: true,
        metadata: {
            type: "point",
            id: title,
            startPoint: startPoint,
            ways: []
        }
    });
    markers.push(marker);
    return marker;
}


function renderDirection(result) {
    var directionDisplay = new google.maps.DirectionsRenderer();
    directionDisplay.setMap(map);
    directionDisplay.setDirections(result);
}

function addPathToPolyLine(result, callBack) {
    var polyline = new google.maps.Polyline({
        strokeColor: '#FF0000',
        strokeOpacity: 0.0001,
        strokeWeight: 3,
        icons: [],
        map: map
    });
    var legs = result.routes[0].legs;
    var totalDistance = 0;
    for (i = 0; i < legs.length; i++) {
        var steps = legs[i].steps;
        totalDistance += legs[i].distance.value;
        for (j = 0; j < steps.length; j++) {
            var nextSegment = steps[j].path;
            for (k = 0; k < nextSegment.length; k++) {
                polyline.getPath().push(nextSegment[k]);
            }
        }
    }
    callBack(polyline, totalDistance);
}

function wayAlternativeTitles(from) {
    var alternatives = [];
    for (var i = 0; i < ways.length; i++) {
        var way = ways[i];
        if (way.from === from) {
            alternatives.push(way.to);
        }
    }
    return alternatives;
}

function animateCircle() {
    window.setInterval(function () {
        var info = '';
        var antInfo = '';
        for (var i = 0; i < ways.length; i++) {
            var way = ways[i];
            var polyline = way.polyline;
            if (polyline) {
                var ants = way.ants;
                var icons = polyline.get('icons');
                if (ants.length > icons.length) {
                    var difference = ants.length - icons.length;
                    for (var j = 0; j < difference; j++) {
                        icons.push({
                            icon: lineSymbol,
                            offset: "0%"
                        });
                    }
                }
                for (var j = 0; j < ants.length; j++) {
                    var ant = ants[j];
                    var distance = way.distance;
                    antInfo += "ANT[" + j + "] WAY[" + i + "]" + " OFFSET" + ant.offset + "<br>";
                    //If ant has finished his last job
                    if (ant.offset === 100) {
                        if (way.pheremone < maxPheremone - 1) {
                            way.pheremone += 1;
                        }
                        var opacity = way.pheremone / maxPheremone;
                        polyline.set('strokeOpacity', opacity > 0 ? opacity : 0.00001);
                        if (way.to === '0') {
                            ant.visited = [];
                        }
                        var visitedPoints = ant.visited;
                        visitedPoints.push(way.to);
                        var differencial = [];
                        for (var k = 0; k < markers.length; k++) {
                            var index = $.inArray(markers[k].title, visitedPoints);
                            if (index === -1)
                                differencial.push(markers[k].title);
                        }
                        var newWay = detectNewWay(differencial, way.to);
                        ant.offset = 0;
                        ants.splice(ants.indexOf(ant), 1);
                        icons.shift();
                        newWay.ants.push(ant);
                    }
                    var location = ((ant.offset + 1) * distance) / 100;
                    location += speed;
                    var newOffset = (location * 100) / distance;
                    if (newOffset > 100) {
                        ant.offset = 100;
                    } else {
                        ant.offset = newOffset;
                    }
                    icons[j].offset = ant.offset + "%";
                    //polyline.set('icons', icons);
                }
                document.getElementById("ants").innerHTML = antInfo;
            }
            if (way.pheremone > 0.005) {
                way.pheremone -= 0.005;
            }
            info += 'WAY= ' + i + ' FROM: ' + way.from + ' TO: ' + way.to + ' Pheremone = ' + way.pheremone + " ANTS = " + way.ants.length + "<br>";
        }
        document.getElementById("info").innerHTML = info;
    }, 20);
}

function getWayByTitle(from, to) {
    for (var i = 0; i < ways.length; i++) {
        var way = ways[i];
        if (way.to === to && way.from === from) {
            return way;
        }
    }
}

function detectNewWay(differencial, currentCheckPointTitle) {
    if (differencial.length === 0) {
        //go back home
        return getWayByTitle(currentCheckPointTitle, '0');
    }
    var alfa = 3;
    var beta = 2;
    var tetaValues = [];
    var sumTeta = 0;

    for (var i = 0; i < differencial.length; i++) {
        var way = getWayByTitle(currentCheckPointTitle, differencial[i]);
        var teta = Math.pow(way.pheremone + 1, alfa) * Math.pow(1 / way.distance, beta);
        sumTeta += teta;
        tetaValues.push(teta);
    }
    var probability = [];
    for (var i = 0; i < tetaValues.length; i++) {
        probability.push(tetaValues[i] / sumTeta);
    }
    
    //Roulette Whell
    var finalRandom = Math.random();
    var index;
    var offset = 0.0;
    for (index = 0; index < probability.length && finalRandom > 0; ++index) {
        offset += probability[index];
        if (finalRandom < offset) {
            break;
        }
    }
    var newWay = getWayByTitle(currentCheckPointTitle, differencial[index]);
    return newWay;
}

google.maps.event.addDomListener(window, 'load', initialize);


